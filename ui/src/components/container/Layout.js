import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import {withStyles} from "@material-ui/core/styles/index";
import NavBar from './NavBar'
import PropTypes from "prop-types";

const styles = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: theme.mixins.toolbar,
    appBarSpacer: theme.mixins.toolbar,
    appBar: {
        position: 'absolute',
        //marginLeft: drawerWidth,
    //    [theme.breakpoints.up('md')]: {
      //      width: `calc(100% - ${drawerWidth}px)`,
      //  },
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto',
    },
});

const Layout = ({children, classes, useTable, onUseTableChange}) => {
    return (
        <React.Fragment>
            <CssBaseline/>
            <div className={classes.root}>
                <AppBar position="absolute" className={classes.appBar}>
                    <NavBar title={"Dashboard"} useTable={useTable} onUseTableChange={onUseTableChange}/>
                </AppBar>
                <main className={classes.content}>
                    <div className={classes.appBarSpacer}/>
                    {children}
                </main>
            </div>
        </React.Fragment>
    );
};

Layout.propTypes = {
    useTable: PropTypes.bool.isRequired,
    onUseTableChange: PropTypes.func.isRequired
};


export default withStyles(styles)(Layout);
