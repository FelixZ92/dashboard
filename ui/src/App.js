import React, {Component} from 'react';
import './App.css';
import Layout from "./components/container/Layout";
import IngressCards from "./components/IngressCards";
import IngressTable from "./components/IngressTable";

class App extends Component {
    state = {
        useTable: false,
    };
    render() {
        return (
            <Layout useTable={this.state.useTable} onUseTableChange={(value) => this.setState({useTable: value})}>
                <div className="App">
                    {this.state.useTable ? <IngressTable/> : <IngressCards/>}
                </div>
            </Layout>
        );
    }
}

export default App;
