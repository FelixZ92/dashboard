PROJECT_NAME := "forward-auth-bouncer-go"
PKG := "gitlab.com/felixz92/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

# Get source files, ignore vendor directory
SRC_FILES := $(shell find . -type f -name '*.go' -not -path "./vendor/*")

.DEFAULT_GOAL := help

.PHONY: help tidy build-backend unit-test-backend golint clean docker dep

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build-backend:
	$(MAKE) -C backend build

unit-test-backend:
	$(MAKE) -C backend unit-test

docker:
	docker build . -t dashboard:latest

golint:
	$(MAKE) -C backend golint

clean: ## Clean binary artifacts
	rm -rf backend/build/_output
	rm -rf ui/build
	rm -rf ui/node_modules
