module gitlab.com/felixz92/dashboard

go 1.15

require (
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/contrib v0.0.0-20190302003538-54ff787f7c73
	github.com/gin-gonic/gin v1.3.0
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/ugorji/go v1.1.8 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/tools v0.0.0-20200917192154-75ebdcb73b49 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	k8s.io/apimachinery v0.19.0
	k8s.io/client-go v0.19.0
)
