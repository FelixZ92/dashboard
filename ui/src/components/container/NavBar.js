import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import {withStyles} from "@material-ui/core/styles/index";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";


const styles = theme => ({
    menuButtonHidden: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    title: {
        flexGrow: 1,
    },
});

const NavBar = ({title, onIconButtonClick, classes, useTable, onUseTableChange}) => {
    return (
        <Toolbar>
            <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={onIconButtonClick}
                className={classes.menuButtonHidden}>
                <MenuIcon/>
            </IconButton>
            <Typography variant="title" color="inherit" noWrap className={classes.title}>
                {title}
            </Typography>
            <FormGroup>
                <FormControlLabel
                    control={<Switch checked={useTable} onChange={(event) => onUseTableChange(event.target.checked)} aria-label="layout switch"/>}
                    label={useTable ? 'Cards' : 'Table'}
                />
            </FormGroup>
            <IconButton color="inherit">
                <Badge badgeContent={0} color="secondary">
                    <NotificationsIcon/>
                </Badge>
            </IconButton>
        </Toolbar>
    );
};

NavBar.propTypes = {
    title: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    useTable: PropTypes.bool.isRequired,
    onUseTableChange: PropTypes.func.isRequired
};


export default withStyles(styles)(NavBar);
