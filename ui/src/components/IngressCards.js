import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import axios from "axios";
import {Button, Card, CardActionArea, CardActions, CardContent} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";

const useStyles = makeStyles((theme) => ({
    container: {
        display: "grid",
        gridTemplateColumns: "repeat(12, 1fr)",
        gridGap: `${theme.spacing.unit * 3}px`
    },
    divider: {
        margin: `${theme.spacing.unit * 2}px 0`
    },
    card: {
        padding: theme.spacing.unit,
        textAlign: "center",
        color: theme.palette.text.secondary,
        whiteSpace: "nowrap",
        marginBottom: theme.spacing.unit
    },
}));

export default function IngressCards() {
    const classes = useStyles();
    const [hasError, setErrors] = useState(false);
    const [data, setIngresses] = useState({ingresses: []});
    useEffect(() => {
            async function fetchData() {
                await axios
                    .get('api/ingresses')
                    .then(result => setIngresses({ingresses: result.data}))
                    .catch(err => setErrors(err));
            }

            fetchData()
        }, []
    )
    return (
        <div>
            <Grid container spacing={1}>
                {data.ingresses.map((row, index) =>
                    <Grid item xs={3} sm={3}>
                        <Card classes={classes.card}>
                            <CardActionArea>
                                <CardContent>
                                    <a href={row.uri} target="_blank"><img src={row.icon} width="200" height="200" alt={row.name}/></a>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {row.name}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions>
                                <Button size="small" color="primary">
                                    <Link href={row.uri} target="_blank">
                                        Open
                                    </Link>
                                </Button>
                                <Button size="small" color="primary">
                                    <Link href={row.docs} target="_blank">
                                        Docs
                                    </Link>
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                )}
            </Grid>
            {hasError ? <span>Has error: {JSON.stringify(hasError)}</span> :<span></span>}
        </div>
    );
}
