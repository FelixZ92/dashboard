package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
)

type DashboardItem struct {
	Name string `json:"name" form:"name" binding:"required"`
	Uri  string `json:"uri" form:"uri" binding:"required"`
	Icon string `json:"icon" form:"icon"`
	Docs string `json:"docs" form:"docs"`
}

var kubeconfig string

var hostRegex = regexp.MustCompile("Host\\(`((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]))`\\)")
var pathPrefixRegex = regexp.MustCompile("PathPrefix\\(`(/[\\w\\d-_]*)`\\)")

func init() {
	flag.StringVar(&kubeconfig, "kubeconfig", "", "path to Kubernetes config file")
	flag.Parse()
}

func main() {

	var config *rest.Config
	var err error

	var staticFilePath string
	if val, ok := os.LookupEnv("STATIC_PATH"); ok {
		staticFilePath = val
	} else {
		staticFilePath = "./ui"
	}

	if kubeconfig == "" {
		log.Printf("flag to kubeconfig not set, looking in environment")
		if val, ok := os.LookupEnv("KUBECONFIG"); ok {
			config, err = clientcmd.BuildConfigFromFlags("", val)
		} else {
			log.Printf("env not set, using in-cluster configuration")
			config, err = rest.InClusterConfig()
		}
	} else {
		log.Printf("using configuration from '%s'", kubeconfig)
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
	}

	if err != nil {
		panic(err)
	}

	err = AddToScheme(scheme.Scheme)
	if err != nil {
		panic(err)
	}

	crdConfig := *config
	crdConfig.ContentConfig.GroupVersion = &schema.GroupVersion{Group: GroupName, Version: GroupVersion}
	crdConfig.APIPath = "/apis"
	crdConfig.NegotiatedSerializer = serializer.NewCodecFactory(scheme.Scheme)
	crdConfig.UserAgent = rest.DefaultKubernetesUserAgent()

	// creates the clientset
	clientset, err := rest.UnversionedRESTClientFor(&crdConfig)
	if err != nil {
		panic(err.Error())
	}

	// Set the router as the default one shipped with Gin
	router := gin.Default()

	// Serve frontend static files
	router.Use(static.Serve("/", static.LocalFile(staticFilePath, true)))

	health := router.Group("/health")
	{
		health.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})
	}

	// Setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/ingresses", func(c *gin.Context) {
			result := IngressRouteList{}
			err := clientset.
				Get().
				Resource("ingressroutes").
				Do(context.TODO()).
				Into(&result)
			if err != nil {
				panic(err.Error())
			}
			var interfaceSlice []DashboardItem
			for i := range result.Items {
				ingress := result.Items[i]
				processRoute := shouldScrape(ingress)
				if processRoute {
					interfaceSlice = append(interfaceSlice, createFromIngressRoute(ingress))
				}
			}
			c.JSON(http.StatusOK, interfaceSlice)
			//c.JSON(http.StatusOK, gin.H{
			//	"ingresses": interfaceSlice,
			//})
		})
	}

	// Start and run the server
	router.Run(":3000")
}

func createFromIngressRoute(route IngressRoute) DashboardItem  {
	item := DashboardItem{}
	url := parseMatch(route.Spec.Routes[0].Match)
	item.Uri = url
	if val, ok := route.ObjectMeta.Annotations["dashboard.zippelf.com/display-name"]; ok {
		item.Name = val
	} else {
		item.Name = route.Name
	}
	if val, ok := route.ObjectMeta.Annotations["dashboard.zippelf.com/logo"]; ok {
		item.Icon = val
	}
	if val, ok := route.ObjectMeta.Annotations["dashboard.zippelf.com/docs"]; ok {
		item.Docs = val
	}

	return item
}

func shouldScrape(ingress IngressRoute) bool {
	if val, ok := ingress.ObjectMeta.Annotations["dashboard.zippelf.com/scrape"]; ok {
		scrape, err := strconv.ParseBool(val)
		if err != nil {
			return false
		}
		return scrape
	}
	return false
}

func parseMatch(match string) string {
	ingressScheme := "https"
	host := parseHost(match)
	prefix := parsePathPrefix(match)
	if host == "" {
		return ""
	}

	if prefix == "" {
		return fmt.Sprintf("%s://%s", ingressScheme, host)
	} else {
		fmt.Println(host)
		return fmt.Sprintf("%s://%s%s", ingressScheme, host, prefix)
	}
}

func parseHost(match string) string {
	hosts := hostRegex.FindStringSubmatch(match)
	if len(hosts) == 0 {
		return ""
	}
	host := hosts[1]
	return host
}

func parsePathPrefix(match string) string {
	prefixes := pathPrefixRegex.FindAllStringSubmatch(match, -1)
	if prefixes == nil {
		return ""
	}
	lastMatchGroup := prefixes[len(prefixes)-1]
	lastMatch := lastMatchGroup[len(lastMatchGroup)-1]
	return fmt.Sprintf("%s/", lastMatch)
}
