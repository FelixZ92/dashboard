FROM alpine:3.9

ADD ui/build /dashboard/ui
ADD backend/build/dashboard /bin/dashboard

RUN chown -R nobody:nogroup /dashboard

USER       nobody
WORKDIR /dashboard
EXPOSE     3000
ENTRYPOINT [ "/bin/dashboard" ]
