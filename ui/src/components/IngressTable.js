import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from "axios";
import Link from '@material-ui/core/Link';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700
    },
});

class IngressTable extends React.Component {
    state = {
        error: null,
        isLoading: false,
        ingresses: [],
        message: null
    };

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        this.setState({isLoading: true});
        axios.get('api/ingresses')
            .then(result => this.setState({
                ingresses: result.data,
                isLoading: false
            }))
            .catch(error => this.setState({
                error: error,
                isLoading: false
            }));
    }

    render() {
        const {classes} = this.props;
        return <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell/>
                        <TableCell>Name</TableCell>
                        <TableCell>URI</TableCell>
                        <TableCell>Docs</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.state.ingresses.map(row => {
                        return (
                            <TableRow key={row.name}>
                                <TableCell><img src={row.icon} width="50" height="50" alt={row.name}/></TableCell>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell>
                                    <Link href={row.uri} className={classes.link}>
                                        {row.uri}
                                    </Link>
                                </TableCell>
                                <TableCell>
                                    <Link href={row.docs} className={classes.link}>
                                        {row.docs}
                                    </Link>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    }
}

IngressTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IngressTable);
